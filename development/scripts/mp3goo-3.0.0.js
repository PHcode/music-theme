/**
 * mp3goo.js v3.0.0
 * @author Fernando Lopez Moreno fernando@zencode.tech
 */

// Loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";

var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// User Configurations
var autoPlayYoutube = true; // YouTube Auto play?
var checkStatus =  {yt: true, external: true}; // Check Status?
var itemName = 'item'; // Item class name.
var messageNotFound = 'Please, Play another Song';

// To store the YT player.
var playerYT;
// To Store the generic player.
var playerEx;
// Current play button.
var currentPlayButton;
// Current index YT.
var currentIndexYT;
// Current index external.
var currentIndexEx;
// Current service
var dataServer;

var mp3goo = {
    init: function () {
        
    }
};

// Start instance from slide
newSlideVolume( $( '.volume-control' ) );

// Start instance from slide
newSlideTime( $( '.time-control' ) );

// Song, play button.
$( '.song .play-button' ).click( function (e) {
    // Prevents the default event.
    e.preventDefault();

    // Activate actual button
    currentPlayButton = activateButton( this );

    // Update download button
    updateDownloadButton();

    // Update song name
    updateSongName();

    // Update controls.
    updateControls( 'play');

    // Play song
    playSong();
} );

// Player, play button.
$( '.player .play-button' ).click( function (e) {
    // Prevents the default event.
    e.preventDefault();

    // Update controls.
    updateControls( 'play');

    // Play song
    playSong();
} );

// Song, pause button.
$( '.song .pause-button' ).click( function (e) {
    // Prevents the default event.
    e.preventDefault();

    // Update controls.
    updateControls( 'pause');

    // Pause song
    pauseSong();
} );

// Player, pause button
$( '.player .pause-button' ).click( function (e) {
    // Prevents the default event.
    e.preventDefault();

    // Update controls.
    updateControls( 'pause');

    // Pause song
    pauseSong();
} );

// Player, stop button.
$( '.player .stop-button' ).click( function (e) {
    // Prevents the default event.
    e.preventDefault();

    // Update controls.
    updateControls( 'stop');

    // Stop song
    stopSong();
} );

// Player, backward song.
$( '.player .prev-button' ).click( function (e) {
    // Prevents the default event.
    e.preventDefault();

    // Store play button with data active prev.
    var currentPrevButton = $( ".song .play-button[data-active='prev']" );

    if ( $( currentPrevButton ).length ) {
        // Activate actual button
        currentPlayButton = activateButton( currentPrevButton );

        // Update download button
        updateDownloadButton();

        // Update song name
        updateSongName();

        // Update controls.
        updateControls( 'play');

        // Play song
        playSong();
    }
} );

// Player, forward song.
$( '.player .next-button' ).click( function (e) {
    // Prevents the default event.
    e.preventDefault();

    // Store play button with data active next.
    var currentNextButton = $( '.song .play-button[data-active="next"]');

    if ( $( currentNextButton ).length ) {
        // Activate actual button
        currentPlayButton = activateButton( currentNextButton );

        // Update download button
        updateDownloadButton();

        // Update song name
        updateSongName();

        // Update controls.
        updateControls( 'play');

        // Play song
        playSong();
    }
} );

// Player, mute song.
$( '.player .volume-button' ).click( function (e) {
    // Prevents the default event.
    e.preventDefault();

    // Mute sound
    muteSong();
} );

/**
 * Trigger when Youtube API is loaded.
 */
function onYouTubeIframeAPIReady() {
    // If there are songs
    if ( $( '.song' ).length ) {
        playerYT = newYTPlayer( 'playerYT' );
    } else {
        // Not song elements
    }
}

/**
 * Create a new instance from API iframe.
 * @param idFrame
 * @return {YT.Player}
 */
function newYTPlayer( idFrame ) {
    return new YT.Player( idFrame, {
        height: '500',
        width: '640',
        events: {
            'onReady': playerReady,
            'onStateChange': playerChange
        }
    });
}

/**
 * Trigger when youtube player is ready.
 */
function playerReady() {
    // Store video list.
    var videoList = [];

    // Iterate all play buttons.
    $( '.song .play-button' ).each( function( index, element ) {
        // Only store YouTube IDs.
        if ( $( element ).attr( 'data-server' ) === 'yt') {
            videoList.push( $( element ).attr( 'data-id' ));
        }
    });

    // Load play list with array IDs
    playerYT.cuePlaylist(videoList);
}

/**
 * Trigger when YT player change.
 */
function playerChange( event ) {
    // Set total length of video.
    setTimeTotal( playerYT.getDuration() );
    // Playing status.
    if (event.data == YT.PlayerState.PLAYING) {
        window.intervalLineTimeYT = setInterval( function () {
            // Set the current time of video
            setCurrentTime( playerYT.getCurrentTime() );

            // Update timer bar per percentage
            $( '.time-control' ).slider( "value", parseInt( playerYT.getCurrentTime() ) / playerYT.getDuration() * 100);
            // Store buffer percentage.
            var buffer = parseInt( playerYT.getVideoLoadedFraction() * 100);
            // Set buffer stored
            $('span.buffer').width(buffer + '%');
        }, 500);
    }

    // Pause status
    if ( event.data == YT.PlayerState.PAUSED ) {
        clearInterval(intervalLineTimeYT);
    }

    // Finish status
    if ( event.data == YT.PlayerState.ENDED ) {
        // Set again at the same index
        playerYT.playVideoAt( currentIndexYT );

        // Clear interval
        clearInterval(intervalLineTimeYT);
        // Stop the video
        playerYT.stopVideo();
        // Clear interval
        clearInterval(intervalLineTimeYT);

        // Update controls
        updateControls('stop');
    }
}

/**
 * Create new instance from slider plugin by Jquery UI.
 * @param el (Object)
 */
function newSlideTime(el) {
    el.slider( {
        range: 'min',
        min: 0,
        value: 0,
        start: function (event, ui) {
            // Clear all intervals.
            if ( dataServer === 'yt' ) {
                clearInterval(intervalLineTimeYT);
            } else if ( dataServer === 'external' ) {
                clearInterval(intervalLineTimeEx);
                playerEx.pause();
            }
        },
        stop: function (event, ui) {
            // Seek to ui value.
            if ( dataServer  === 'yt' ) {
                playerYT.seekTo( playerYT.getDuration() / 100 * ui.value, true );
            } else if ( dataServer === 'external' ) {
                playerEx.currentTime = parseInt( playerEx.duration ) / 100 * parseInt( ui.value );
                // Play audio.
                playerEx.play();
            }
        }
    } ).prepend( '<span class="buffer"></span>' );
}

/**
 * Create new instance from slider plugin by Jquery UI.
 * @param el {Object}
 */
function newSlideVolume(el) {
    // Apply styles and configs to slider
    el.slider({
        range: "min",
        min: 0,
        value: 100,
        start: function(event,ui) {
            // Start
        },
        slide: function(event, ui) {
            /* Get slider value - volume value */
            var value = el.slider('value');

            /* Element is obtained to show the image of speaker according to level of volume */
            var volume = $(' .volume-button ');

            /* Change position of speak sprite png */
            if(ui.value <= 5) {
                volume.css('background-position', '0 0');
            }
            else if (ui.value <= 25) {
                volume.css('background-position', '0 -25px');
            }
            else if (ui.value <= 75) {
                volume.css('background-position', '0 -50px');
            }
            else {
                volume.css('background-position', '0 -75px');
            }

            // Adjust volume song.
            volumeSong( ui.value );
        },
        stop: function(event,ui) {
            // Stop
        },
        change: function( event, ui ) {
            /* Element is obtained to show the image of speaker according to level of volume */
            var volume = $(' .volume-button ');

            /* Change position of speak sprite png */
            if(ui.value <= 5) {
                volume.css('background-position', '0 0');
            }
            else if (ui.value <= 25) {
                volume.css('background-position', '0 -25px');
            }
            else if (ui.value <= 75) {
                volume.css('background-position', '0 -50px');
            }
            else {
                volume.css('background-position', '0 -75px');
            }
        }
    });
}

/**
 * Activate the clicked button.
 * @param playButtonClicked {Object} Button clicked
 */
function activateButton(playButtonClicked) {
    // Reset to false data-active attribute
    $( '.song .play-button' ).attr( 'data-active', 'false' );

    // Add active attribute
    $( playButtonClicked ).attr( 'data-active', 'true' );

    // Add prev and next values
    $( playButtonClicked ).parent().parent().prev().find( '.play-button' ).attr( 'data-active', 'prev' );
    $( playButtonClicked ).parent().parent().next().find( '.play-button' ).attr( 'data-active', 'next' );

    // Return current play button
    return $( ".play-button[data-active='true']" );
}

/**
 * Update controls from audio player.
 * @param status {String} Name of status.
 */
function updateControls(status) {
    if (status === 'play') {
        // Show all play buttons
        $( '.song .play-button' ).show();
        // Hide play button
        $( currentPlayButton ).hide();
        // Hide all pause buttons
        $( '.song .pause-button' ).hide();
        // Show this pause button
        $( currentPlayButton ).next().show();

        // Hide play buttom from player
        $( '.player .play-button' ).hide();
        // Show pause button from player
        $( '.player .pause-button' ).show();
    } else if (status === 'pause') {
        // Hide pause button
        $( currentPlayButton ).next().hide();
        // Show play button
        $( currentPlayButton ).show();

        // Hide pause button from player
        $( '.player .pause-button' ).hide();
        // Show play buttom from player
        $( '.player .play-button' ).show();
    } else if (status === 'stop') {
        // Update timer bar per percentage
        $( '.time-control' ).slider( "value", '0');

        // Hide pause button
        $( currentPlayButton ).next().hide();
        // Show play button
        $( currentPlayButton ).show();

        // Hide pause button from player
        $( '.player .pause-button' ).hide();
        // Show play buttom from player
        $( '.player .play-button' ).show();
    }
}

/**
 * Start the call to play the song.
 */
function playSong() {
    // Show player
    $( '.player-data' ).fadeIn( 'slow' );
    $( '.player-container' ).fadeIn( 'slow' );

    // If previously exist data-server
    if ( dataServer === 'yt' ) {
        // Then stop yt.
        stopYoutube();
        // Clear old interval
        clearInterval( intervalLineTimeYT );
    } else if ( dataServer === 'external' && $( currentPlayButton ).attr( 'data-server' ) === 'external' ) {
        // Then stop external.
        stopExternal( 'soft' );
        // Clear old interval
        clearInterval( intervalLineTimeEx );
    } else if ( dataServer === 'external' && $( currentPlayButton ).attr( 'data-server' ) === 'yt' ) {
        // Then stop external.
        stopExternal( 'hard' );
        // Clear old interval
        clearInterval( intervalLineTimeEx );
    }
    // Get current service
    dataServer = $( currentPlayButton ).attr( 'data-server' );

    if (dataServer === 'yt') {
        // Then play YT video.
        playYoutube();
    } else if (dataServer === 'external') {
        // Then play Ex audio.
        playExternal();
    }
}

/**
 * Play one video from YT.
 * @return {boolean} If is played or not.
 */
function playYoutube() {
    // Get current data id.
    var dataId = $( currentPlayButton ).attr( 'data-id' );
    // Store index of video.
    var position = 0;
    // If video is not matched set true.
    var notMatched = false;

    // Iterate all play buttons.
    $( '.song .play-button' ).each( function ( index, element ) {
        // Only store YouTube IDs.
        if ( $( element ).attr( 'data-server' ) === 'yt') {
            // Found video.
            if ( $( element ).attr( 'data-id' ) === dataId && $.inArray( dataId, playerYT.getPlaylist() ) !== -1) {
                // Video is matched.
                notMatched = false;
                // Stop loop.
                return false;
            } else {
                // Video is not found.
                notMatched = true;
            }

            // Sum position
            position++;
        }
    });

    // Do things if the video meets or not.
    if (!notMatched) {
        if (currentIndexYT == position) {
            // Play video after it has been paused.
            playerYT.playVideo();
        } else {
            // Start video in the position obtained.
            playerYT.playVideoAt(position);
            // Store actual index
            currentIndexYT = position;
        }
    } else {
        // Item not found
        notFound();
    }
}

/**
 * Play one audio from external source.
 */
function playExternal() {
    // Get current data id.
    var currentSrc = $( currentPlayButton ).parent().siblings( '.external' )[0].currentSrc;
    // Store index of current src.
    var position = 0;
    // If the source is not matched set true.
    var notMatched = false;

    // Iterate all audio players.
    $( '.external' ).each( function ( index, element ) {
        // Only store external source.
        if ( $( element )[0].currentSrc === currentSrc ) {
            // Audio is matched.
            notMatched = false;
            // Stop loop.
            return false;
        } else {
            notMatched = true;
        }
        // Add one to the current position
        position++;
    });

    // Do things if the music meets or not.
    if ( ! notMatched ) {
        if (currentIndexEx == position) {
            // Play audio
            playerEx.play();
        } else {
            // Delete old events
            $( playerEx ).unbind();
            // Store audio player external.
            playerEx = $('.external')[position];
            // Reload audio content
            playerEx.load();
            // Play audio
            playerEx.play();
            // Fires when meta data can setting.
            playerEx.onloadeddata = function () {
                // Set duration
                setTimeTotal( parseInt( playerEx.duration ) );
            };
            // 	Fires when the audio/video has been started or is no longer paused.
            playerEx.onplay = function () {
                // Set interval for slide seek.
                window.intervalLineTimeEx = setInterval(function(){
                    // Set duration
                    setTimeTotal( parseInt( playerEx.duration ) );

                    // Update timer bar per percentage
                    $( '.time-control' ).slider( "value", parseInt( playerEx.currentTime ) / playerEx.duration * 100);
                    // Set  buffer size.
                    if( playerEx.buffered ){
                        if( playerEx.buffered.length > 0 ){
                            if( playerEx.buffered.length === 1 ){
                                var bufferedEnd = playerEx.buffered.end(0);
                            } else {
                                var bufferedEnd = playerEx.buffered.end( playerEx.buffered.length - 1);
                            }

                            // var duration =  audioPlayer.duration;
                            var buffer = ( bufferedEnd / playerEx.duration ) * 100;

                            $('span.buffer').width(buffer + '%');
                        }
                    }
                }, 500);
            };
            /**
             * @description MP3 - Event Slide. Trigger when the audio is paused.
             * @param e {object}
             */
            playerEx.onpause = function(e) {
                //Clear Interval
                clearInterval(intervalLineTimeEx);
            };
            // The timeupdate event occurs when the playing position of an audio/video has changed.
            playerEx.ontimeupdate = function () {
                // Set current time
                setCurrentTime( playerEx.currentTime );
            };
            // Fires event when the current playlist is ended.
            playerEx.onended = function() {
                // Clear interval
                clearInterval(intervalLineTimeEx);
                // Update controls
                updateControls('stop');
            };

            // Fires when an error occurred during the loading of an audio/video.
            $( playerEx ).children().unbind().bind( 'error', function () {
                // Item not found.
                notFound();
            });

            // Store actual index
            currentIndexEx = position;
        }
    } else {
        // Item not found.
        notFound();
    }
}

/**
 * Announces to the user that the song was not found and the item is deleted.
 */
function notFound() {
    var item = $( currentPlayButton ).parent().parent();

    $( item ).html( '<p class="message-not-found">' + messageNotFound + '</p>');

    // Delete item after 3 seconds.
    setTimeout( function () {
        $( item ).slideUp( 'slow', function() {
            // Remove item
            $( item ).remove();
        });
    }, 3000);
}

/**
 * Start the call to pause the song.
 */
function pauseSong() {
    // Pause music according to service.
    if (dataServer === 'yt') {
        // Then pause YT video.
        pauseYoutube();
    } else if (dataServer === 'external') {
        // Then pause external audio.
        pauseExternal();
    }
}

/**
 * Pause selected YouTube video.
 */
function pauseYoutube() {
    if ( playerYT.getPlayerState() === 1 ) {
        // Pause video
        playerYT.pauseVideo();
    }
}

/**
 * Pause external audio.
 */
function pauseExternal() {
    playerEx.pause();
}

/**
 * Start the call to stop the song.
 */
function stopSong() {
    // Stop music according to service.
    if (dataServer === 'yt') {
        // Then stop YT video.
        stopYoutube();
    } else if (dataServer === 'external') {
        // Then stop external audio.
        stopExternal( 'hard' );
    }
}

/**
 * Stop the YT video.
 */
function stopYoutube() {
    if ( playerYT.getPlayerState() === 1 ) {
        // Pause video
        playerYT.stopVideo();
    }
}

/**
 * Stop the external audio.
 */
function stopExternal( mode ) {
    if ( mode === 'soft' ) {
        playerEx.pause();
    } else if ( mode === 'hard' ) {
        playerEx.pause();
        playerEx.currentTime = 0;
    }
}

/**
 * Start the call to Adjust volume.
 */
function volumeSong( value ) {
    // Adjust music volume according to service.
    if (dataServer === 'yt') {
        // Then adjust volume YT video.
        volumeYoutube( value );
    } else if (dataServer === 'external') {
        // Then adjust volume external audio.
        volumeExternal( value );
    }
}

/**
 * Adjust volume from YT video.
 * @param value {Number} Value between 0 and 100.
 */
function volumeYoutube( value ) {
    playerYT.setVolume( value );
}

/**
 * Adjust volume from external audio.
 * @param value {Number} Value between 0 and 100.
 */
function volumeExternal( value ) {
    playerEx.volume = value / 100;
}

/**
 * Update the download button from player.
 */
function updateDownloadButton() {
    var downloadButton = $( currentPlayButton ).siblings( '.download-button' );

    // Set new url for download
    $( '.player .download-button' ).attr( 'href', $( downloadButton ).attr( 'href' ) );
}

/**
 * Update the name song from player data.
 */
function updateSongName() {
    var songName = $( currentPlayButton )
        .parent()
        .siblings( '.labels' )
        .find( '.title-song' )
        .html();

    // Set new url for download
    $( '.player-data .song-name' ).html( songName );
}

/**
 * Set volume to zero.
 */
function muteSong() {
    // Mute music according to service.
    if (dataServer === 'yt') {
        // Then mute YT video.
        playerYT.setVolume( 0 );
    } else if (dataServer === 'external') {
        // Then mute external audio.
        playerEx.volume = 0;
    }

    // Update volume slide
    $( '.volume-control' ).slider( 'value', '0');
}

/**
 * Set the total time from song.
 * @param secondsParam {Number} Integer.
 */
function setTimeTotal( secondsParam ) {
    // Store total duration
    var duration = secondsParam;
    // Store minutes
    var minutes = parseInt(duration / 60);
    // Store seconds
    var seconds = parseInt(duration % 60);
    // Store hours
    var hours = parseInt( duration / 3600);
    // For store the total time of duration
    var timeDuration;

    // Time format is created.
    if(minutes.toString().length == 1) {
        if( seconds.toString().length == 1 ) {
            timeDuration = '0' + minutes + ':' + '0' + seconds;
        } else {
            timeDuration = '0' + minutes + ':' + seconds;
        }
    } else {
        if( seconds.toString().length == 1 ) {
            timeDuration = minutes + ':' + '0' + seconds;
        } else {
            timeDuration = minutes + ':' + seconds;
        }
    }

    // Format hours
    if ( hours >= 1 ) {
        if ( hours.toString().length === 1 ) {
            timeDuration = '0' + hours + ':' + timeDuration;
        } else {
            timeDuration = hours + ':' + timeDuration;
        }
    }

    // Set duration formatted
    $( '.player-data .time-total' ).html( timeDuration );
}

/**
 * Format and set the current tima of the song.
 * @param seconds {Number} Seconds of current tiem.
 */
function setCurrentTime( seconds ) {
    // Store seconds
    var secondsTimer = parseInt( seconds % 60 );
    // Store minutes
    var minutes = parseInt( seconds / 60 )
    // Store hours
    var hours = parseInt( seconds / 3600);

    // Set the seconds.
    if( secondsTimer.toString().length == 1 ) {
        $( '.player-data .seconds' ).html( '0' + secondsTimer );
    } else {
        $( '.player-data .seconds' ).html( secondsTimer );
    }

    // Set the minutes.
    if( minutes.toString().length == 1 ) {
        $( '.player-data .minutes' ).html('0' + minutes);
    } else {
        $( '.player-data .minutes' ).html( minutes );
    }

    // Set the seconds
    if ( hours >= 1 ) {
        $( '.player-data .hours' ).show();
        if( hours.toString().length == 1 ) {
            $( '.player-data .hours' ).html('0' + hours + ':');
        } else {
            $( '.player-data .hours' ).html( hours + ':');
        }
    } else {
        $( '.player-data .hours' ).hide();
    }
}
