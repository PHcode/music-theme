// Header ajax
var headerAjax = {
    loaderAjax: '<div id="loaderAjax"><div class="icon"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> </div> </div>',
    eventActive: false,
    setPagePage: function ( content ) {
        // Remove previous content
        $( '.content-home' ).remove();
        $( '.content-search' ).remove();
        $( '.content-page' ).remove();
        $( '.content-404' ).remove();
        // Append search content
        $( '.main-content' ).append( content );
        // Refresh events
        this.createEvents();
    },
    getPagePage: function ( pageId ) {
        // Store context
        var that = this;
        // Ajax request
        $.ajax({
            url: wp.ajaxUrl,
            method: 'POST',
            dataType: 'json',
            data: {
                action: 'pagePage',
                pageId: pageId
            },
            beforeSend: function (jqXHR) {
                // Add loader screen
                $( '.main-content' ).append( that.loaderAjax );
            },
            error: function (jqXHR) {
                console.log( jqXHR );
            },
            success: function (data, textStatus, jqXHR) {
                // Change URL
                window.history.pushState(null, null, data.permalink.replace(/^.*\/\/[^\/]+/, ''));
                // Remove loader screen
                $( '#loaderAjax' ).remove();
                // Turn off event indicator.
                that.eventActive = false;
                // Set search content
                that.setPagePage( data.content );
            }
        });
    },
    homePaged: 1,
    setHomePage: function ( content ) {
        // Remove previous content
        $( '.content-home' ).remove();
        $( '.content-search' ).remove();
        $( '.content-page' ).remove();
        $( '.content-404' ).remove();
        // Append search content
        $( '.main-content' ).append( content );
        // Refresh events
        this.createEvents();
    },
    getHomePage: function ( paged ) {
        // Store context
        var that = this;
        // Ajax request
        $.ajax({
            url: wp.ajaxUrl,
            method: 'POST',
            dataType: 'json',
            data: {
                action: 'homePage',
                paged: paged
            },
            beforeSend: function (jqXHR) {
                // Add loader screen
                $( '.main-content' ).append( that.loaderAjax );
            },
            error: function (jqXHR) {
                console.log( jqXHR );
            },
            success: function (data, textStatus, jqXHR) {
                // Change URL
                window.history.pushState(null, null, wp.siteUrl);
                // Remove loader screen
                $( '#loaderAjax' ).remove();
                // Turn off event indicator.
                that.eventActive = false;
                // Set search content
                that.setHomePage( data.content );
            }
        });
    },
    searchPaged: 1,
    searchWord: '',
    setSearchPage: function ( content ) {
        // Remove previous content
        $( '.content-home' ).remove();
        $( '.content-search' ).remove();
        $( '.content-page' ).remove();
        $( '.content-404' ).remove();
        // Append search content
        $( '.main-content' ).append( content );

        // Refresh events
        this.createEvents();
    },
    getSearchPage: function ( word, paged ) {
        // Store context
        var that = this;
        // Ajax request
        $.ajax({
            url: wp.ajaxUrl,
            method: 'POST',
            dataType: 'json',
            data: {
                action: 'searchPage',
                word: word,
                paged: paged
            },
            beforeSend: function (jqXHR) {
                // Add loader screen
                $( '.main-content' ).append( that.loaderAjax );
            },
            error: function (jqXHR) {
                console.log( jqXHR );
            },
            success: function (data, textStatus, jqXHR) {
                // Change URL
                //window.history.pushState(null, null, data.the_permalink.replace(/^.*\/\/[^\/]+/, ''));
                // Remove loader screen
                $( '#loaderAjax' ).remove();
                // Turn off event indicator.
                that.eventActive = false;
                // Set search content
                that.setSearchPage( data.content );
            }
        });
    },
    createEvents: function () {
        // Store context
        var that = this;

        // Capture search event from first search box.
        $( '#searchFormOne' ).submit( function (e) {
            // Prevent default event.
            e.preventDefault();
            // Prevent double click
            if ( !that.eventActive ) {
                that.eventActive = true;
                that.searchWord = $( '#s' ).val();
                if ( that.searchWord !== '' ) {
                    that.getSearchPage( that.searchWord, 1 );
                }
            }
        } );

        // Capture search event from second search box.
        $( '#searchFormTwo' ).submit( function (e) {
            // Prevent default event.
            e.preventDefault();
            // Prevent double click
            if ( !that.eventActive ) {
                that.eventActive = true;
                that.searchWord = $( '#search' ).val();
                if ( that.searchWord !== '' ) {
                    that.getSearchPage( that.searchWord, 1 );
                }
            }
        } );

        // Capture search navigation event.
        $( '.content-search a.page-numbers' ).click( function (e) {
            // Prevent default event.
            e.preventDefault();
            // Prevent double click
            if ( !that.eventActive ) {
                that.eventActive = true;
                // Check if it is a number
                if ( $( this ).hasClass( 'next' ) ) {
                    that.searchPaged++;
                } else if ( $( this ).hasClass( 'prev' ) ) {
                    that.searchPaged--;
                } else {
                    that.searchPaged = $( this ).html();
                }
                that.getSearchPage( that.searchWord, that.searchPaged );
            }
        } );

        // Capture logo link event.
        $( '.custom-logo-link' ).click( function (e) {
            // Prevent default event.
            e.preventDefault();
            // Prevent double click
            if ( !that.eventActive ) {
                that.eventActive = true;
                that.getHomePage( that.homePaged );
                // Refresh active class.
                $( '.menu-item' ).removeClass( 'active' );
                var menuButton = $( ' .menu-item a[href="' + wp.siteUrl + '"] ' );
                if ( ! $( menuButton ).length ) {
                    menuButton = $( ' .menu-item a[href="' + wp.siteUrl + '/"] ' );
                }
                $( menuButton ).parent().addClass( 'active' );
            }
        } );

        // Capture event from the main menu.
        $( '.menu-item a' ).click( function (e) {
            // Prevent default event.
            e.preventDefault();
            // Prevent double click
            if ( !that.eventActive ) {
                that.eventActive = true;
                // Check if it's home
                if ( $( this ).attr( 'href' ) == ( wp.siteUrl + '/' ) || $( this ).attr( 'href' ) == wp.siteUrl ) {
                    that.getHomePage( that.homePaged );
                } else {
                    var pageId = $( this ).attr( 'data-id' );
                    that.getPagePage( pageId );
                }
                // Refresh active class.
                $( '.menu-item' ).removeClass( 'active' );
                $( this ).parent().addClass( 'active' );
            }
        } );

    },
    init: function () {
        this.createEvents();
    }
};

headerAjax.init();