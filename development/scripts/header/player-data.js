// Player data 
var playerData = {
    init: function () {
        $( '.song .play-button' ).click( function (e) {
            // Prevents the default event.
            e.preventDefault();
            // Store song name element
            var songName = $( '.player-data .song-name' );
            // Store the text
            var songNameText = songName.text();
            // Store the actual window width.
            var windowWidth = $( window ).width();

            // Add custom class according conditions
            if ( songNameText.length > 25 && windowWidth <= 400) {
                $( songName ).addClass( 'add-points' );
            } else {
                $( songName ).removeClass( 'add-points' );
            }
            // Add custom class according conditions
            if ( songNameText.length > 32 && windowWidth <= 500) {
                $( songName ).addClass( 'add-points' );
            } else {
                $( songName ).removeClass( 'add-points' );
            }
            // Add custom class according conditions
            if ( songNameText.length > 37 && windowWidth <= 600) {
                $( songName ).addClass( 'add-points' );
            } else {
                $( songName ).removeClass( 'add-points' );
            }
        } );
    }
};

// Start player data object
playerData.init();