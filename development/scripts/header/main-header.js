// Main header
var mainHeader = {
    init: function () {
        if ( $( window ).width() <= 768 ) {
            $( '.header-fixed .form-control' ).attr( 'placeholder', 'Search...' );
        }
        $( window ).resize( function () {
            if ( $( window ).width() <= 768 ) {
                $( '.header-fixed .form-control' ).attr( 'placeholder', 'Search...' );
            } else {
                $( '.header-fixed .form-control' ).attr( 'placeholder', 'Search by Artist - Song' );
            }
        } );


        $(window).scroll(function (event) {
            var scroll = $(window).scrollTop();
            // Do something
            if (scroll >= 155 ) {
                $( '.header-fixed' ).fadeIn();
            } else {
                $( '.header-fixed' ).fadeOut();
            }
        });
    }
};

// Start main header object
mainHeader.init();