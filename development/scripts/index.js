//=include ../../bower_components/jquery/dist/jquery.js
//=include ../../bower_components/bootstrap/dist/js/bootstrap.js
//=include ../../bower_components/jquery-ui/jquery-ui.js
//=include ../../bower_components/jqueryui-touch-punch/jquery.ui.touch-punch.js
//=include mp3goo-3.0.0.js
//=include header/main-header.js
//=include header/player-data.js
//=include header/header-ajax.js

(function( $ ) {
    /**
     * Navigation dropdown
     */
    $( '.dropdown > a' ).attr('data-toggle', 'dropdown')
        .attr( 'role', 'button' )
        .attr( 'aria-haspopup', 'true' )
        .attr( 'aria-expanded', 'false')
        .click( function (e) {
            e.preventDefault();
        } );

    /**
     * Data site
     */
    $( '.name-description' ).animate({
        left: $( '.logo-header' ).width() + 10
    }, 300, 'swing', function () {
        // Animation complete
        $( '.logo-header' ).fadeIn()
            .find('img')
            .attr('alt', wp.blogInfoSite);
    }); 

    /**
     * Search form
     */
    if ($( window ).width() < 768) {
        var newWidth = $( '#searchform' ).width() - 69;
        $( '#searchform' ).width(newWidth);
    }

    $( window ).resize( function () {
        if ($( window ).width() < 768) {
            var newWidth = $( '#searchform' ).parent().width() - 69;
            $( '#searchform' ).width(newWidth);
        } else {
            $( '#searchform' ).css( 'width', '100%' );
        }
    } );

    /**
     * Main navigation
     */
    if ( $( window ).width() < 768 ) {
        var newWidth = $( '.main-nav' ).parent().width();
        $( '.main-nav' ).width(newWidth);
    }

    $( window ).resize( function () {
        if ($( window ).width() < 768) {
            var newWidth = $( '.main-nav' ).parent().width();
            $( '.main-nav' ).width(newWidth);
        } else {
            $( '.main-nav' ).css( 'width', '100%' );
        }
    } );


})( jQuery );

