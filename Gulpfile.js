/**
 * Created by phant on 3/14/2017.
 */

var gulp = require('gulp');
var sass = require("gulp-sass");
var minifyCss = require("gulp-minify-css");
var include = require("gulp-include");
var watch = require("gulp-watch");
var minify = require('gulp-minify');

gulp.task("default", ["scripts", "styles"]);

gulp.task("styles", function () {
    return gulp.src("./development/styles/index.scss")
        .pipe(sass().on("error", sass.logError))
        .pipe(minifyCss({
            keepSpecialComments: 0
        }))
        .pipe(gulp.dest("./wp-content/themes/music-theme/assets/css/"));
});

gulp.task("scripts", function() {
    gulp.src(["./development/scripts/index.js"])
        .pipe(include())
        .pipe(minify({
            ext:{
                min:'.min.js'
            }
        }))
        .pipe(gulp.dest("./wp-content/themes/music-theme/assets/js/"));
});

gulp.task("watch", function () {
    gulp.watch('./development/styles/*.scss', ['styles'] );
    gulp.watch('./development/scripts/*.js', ['scripts'] );
});