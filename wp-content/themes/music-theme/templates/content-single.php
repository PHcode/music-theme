<?php
/**
 * Template part for displaying content from home page.
 * @package MusicTheme
 * @since 1.0.0
 * @version 1.0.0
 */
?>
<article class="post">
    <div class="title">
        <h1><?php the_title(); ?></h1>
    </div>
    <div class="data">
        <div class="author">
            <i class="fa fa-user" aria-hidden="true"></i> <?php the_author(); ?>
        </div>
        <div class="date">
            <i class="fa fa-calendar" aria-hidden="true"></i> <?php echo get_the_date( 'l, F j, Y' ); ?>
        </div>
        <div class="tags">
            <?php the_tags( '<i class="fa fa-tags" aria-hidden="true"></i> ', ', ', '' ); ?>
        </div>
    </div>
    <div class="social-sharing-buttons">
        <a class="facebook-button" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink(); ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        <a class="twitter-button" href="https://twitter.com/home?status=<?php echo urlencode( get_the_permalink() ); ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
        <a class="google-plus-button" href="https://plus.google.com/share?url=<?php echo urlencode( get_the_permalink() ); ?>" target="_blank"><i class="fa fa-google" aria-hidden="true"></i></a>
    </div>
    <figure class="featured-image">
        <?php if ( has_post_thumbnail() ) {
            the_post_thumbnail( 'music_theme-thumbnail' );
        } else {
            echo '<img src="' . get_bloginfo( 'stylesheet_directory' ) . '/assets/images/no-cover-image.png" />';
        } ?>
    </figure>
    <div class="content">
        <?php echo get_the_content(); ?>
    </div>
</article>
