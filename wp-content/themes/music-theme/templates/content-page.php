<?php
/**
 * Template part for displaying content from page.
 * @package MusicTheme
 * @since 1.0.0
 * @version 1.0.0
 */
?>
<article class="post">
    <header class="title">
        <h1><?php the_title(); ?></h1>
    </header>
    <?php if ( has_post_thumbnail(  ) ) : ?>
        <figure class="featured-image">
            <?php the_post_thumbnail( 'large' ); ?>
        </figure>
    <?php endif; ?>
    <div class="content">
        <?php echo wpautop( get_the_content() ); ?>
    </div>
</article>