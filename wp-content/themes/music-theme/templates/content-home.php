<?php
/**
 * Template part for displaying content from home page.
 * @package MusicTheme
 * @since 1.0.0
 * @version 1.0.0
 */
?>
<div class="post">
    <figure class="featured-image">
        <?php if ( has_post_thumbnail() ) {
            the_post_thumbnail( 'music_theme-thumbnail' );
        } else {
            echo '<img src="' . get_bloginfo( 'stylesheet_directory' ) . '/assets/images/no-cover-image.png" />';
        } ?>
    </figure>
    <div class="summary-data">
        <div class="title">
            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        </div>
        <div class="author">
            <p><i class="fa fa-user" aria-hidden="true"></i> <?php the_author(); ?></p>
        </div>
        <div class="date">
            <p><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo get_the_date( 'l, F j, Y' ); ?></p>
        </div>
    </div>
</div>
