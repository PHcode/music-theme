<?php
/**
 * Template part for displaying message from not content.
 * @package MusicTheme
 * @since 1.0.0
 * @version 1.0.0
 */
?>
<div class="post">
    <p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'music-theme' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>
</div>

