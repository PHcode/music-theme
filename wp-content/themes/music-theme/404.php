<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package MusicTheme
 * @since 1.0.0
 * @version 1.0.0
 */
get_header(); ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="main-content">
                    <!-- Content 404 -->
                    <section class="content-404">
                        <header class="header-404">
                            <h1 class="title-404">
                                <?php _e( 'Oops! That page can&rsquo;t be found.', 'music_theme' ); ?>
                            </h1>
                        </header>
                        <article class="post">
                            <p>
                                <?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'music_theme' ); ?>
                            </p>
                            <form class="search-form-header" role="search" method="get" id="searchform" action="<?php echo get_site_url(); ?>">
                                <div class="form-group">
                                    <label for="search"><?php esc_html_e( 'Search by Artist - Song', 'music_theme' ); ?></label>
                                    <input type="search" class="form-control" name="s" id="s" placeholder="Search by Artist - Song">
                                </div>
                                <button type="submit" class="btn btn-default submit" id="searchsubmit">Search</button>
                            </form>
                        </article>
                    </section><!-- ./Content 404 -->
                </div>
            </div>
        </div>
    </div>
<?php get_footer();

