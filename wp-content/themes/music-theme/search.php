<?php
/**
 * The template for displaying search results pages
 *
 * @package MusicTheme
 * @since 1.0.0
 * @version 1.0.0
 */
get_header(); ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="main-content">
                    <section class="content-search">
                        <header class="header-search">
                            <?php if ( have_posts() ) : ?>
                                <h1 class="title-search">
                                    <?php printf( __( 'Search results for: %s', 'music_theme' ), '<span>' . get_search_query() . '</span>' ); ?>
                                </h1>
                            <?php else : ?>
                                <h1 class="title-search">
                                    <?php _e( 'Nothing Found', 'music_theme' ); ?>
                                </h1>
                            <?php endif; ?>
                        </header>
                        <?php if ( have_posts() ) : ?>
                            <?php while ( have_posts() ) : the_post();
                                get_template_part( 'templates/content', 'search' );
                            endwhile;
                            the_posts_pagination( array(
                                'prev_text' => '<i class="fa fa-angle-double-left" aria-hidden="true"></i>',
                                'next_text' => '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
                                'before_page_number' => '',
                            ) );
                        else: ?>
                            <p class="nothing-matched">
                                <?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'music_theme' ); ?>
                            </p>
                        <?php endif; ?>
                    </section>
                </div>
            </div>
        </div>
    </div>
<?php get_footer();