<?php
/**
 * The template for displaying the footer
 * @package MusicTheme
 * @since 1.0.0
 * @version 1.0.0
 */

?>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <footer class="main-footer">
                <p>
                    © 2017 <a href="<?php echo get_site_url(); ?>">Music theme</a> by Fernando López.
                </p>
            </footer>
            <!-- Music player -->
            <div class="player-container">
                <div class="player">
                    <div class="left-controls">
                        <a href="#" target="_blank" class="download-button"><i class="fa fa-download" aria-hidden="true"></i></a>
                        <button class="stop-button"><i class="fa fa-stop" aria-hidden="true"></i></button>
                    </div>
                    <div class="center-controls">
                        <button class="prev-button"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
                        <button class="play-button"><i class="fa fa-play" aria-hidden="true"></i></button>
                        <button class="pause-button"><i class="fa fa-pause" aria-hidden="true"></i></button>
                        <button class="next-button"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
                    </div>
                    <div class="right-controls">
                        <button class="volume-button"></button>
                        <div class="volume-control"></div>
                    </div>
                </div>
                <div class="player-time">
                    <div class="time-control"></div>
                </div>
            </div><!-- ./Music player -->
        </div>
    </div>
</div>
<!-- iFrame for YouTube API -->
<iframe id="playerYT" type="text/html" width="640" height="360" style="position: absolute; left: -99999px"
        src="https://www.youtube.com/embed/?enablejsapi=1"
        frameborder="0"></iframe>
<?php wp_footer(); ?>
</body>
</html>
