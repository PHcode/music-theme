<?php
/**
 * Music Theme functions and definitions
 * @package MusicTheme
 * @since 1.0.0
 * @version 1.0.0
 */

/**
 * Music Theme only works in WordPress 4.7 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

/**
 * Create new instance form request ajax class.
 */
require get_template_directory() . '/inc/class-music-theme-request-ajax.php';

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function music_theme_setup() {
	/*
	 * Make theme available for translation.
	 */
	load_theme_textdomain( 'music_theme' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'music_theme-cover', 200, 200, true );
    add_image_size( 'music_theme-thumbnail', 100, 100, true );

	// Set the default content width.
	$GLOBALS['content_width'] = 1000;

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'top'    => __( 'Top Menu', 'music_theme' )
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo', array(
		'width'       => 180,
		'height'      => 80,
		'flex-width'  => true,
	) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}

add_action( 'after_setup_theme', 'music_theme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function music_theme_content_width() {

	$content_width = $GLOBALS['content_width'];

	// Get layout.
	$page_layout = get_theme_mod( 'page_layout' );

	// Check if layout is one column.
	if ( 'one-column' === $page_layout ) {
		if ( music_theme_is_frontpage() ) {
			$content_width = 644;
		} elseif ( is_page() ) {
			$content_width = 740;
		}
	}

	// Check if is single post and there is no sidebar.
	if ( is_single() && ! is_active_sidebar( 'sidebar-1' ) ) {
		$content_width = 740;
	}

	/**
	 * Filter Music Theme content width of the theme.
	 *
	 * @since 1.0.0
	 * @param $content_width integer
	 */
	$GLOBALS['content_width'] = apply_filters( 'music_theme_content_width', $content_width );
}
add_action( 'template_redirect', 'music_theme_content_width', 0 );


/**
 * Add preconnect for Google Fonts.
 *
 * @since 1.0.0
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function music_theme_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'music_theme-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'music_theme_resource_hints', 10, 2 );

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since 1.0.0
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function music_theme_excerpt_more( $link ) {
	if ( is_admin() ) {
		return $link;
	}

	$link = sprintf( '<p class="link-more"><a href="%1$s" class="more-link">%2$s</a></p>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Name of current post */
		sprintf( __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'music_theme' ), get_the_title( get_the_ID() ) )
	);
	return ' &hellip; ' . $link;
}
add_filter( 'excerpt_more', 'music_theme_excerpt_more' );

/**
 * Handles JavaScript detection.
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since 1.0.0
 */
function music_theme_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}

add_action( 'wp_head', 'music_theme_javascript_detection', 0 );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function music_theme_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">' . "\n", get_bloginfo( 'pingback_url' ) );
	}
}
add_action( 'wp_head', 'music_theme_pingback_header' );

/**
 * Enqueue scripts and styles.
 */
function music_theme_scripts() {
    wp_enqueue_script( 'pace-js', get_template_directory_uri() . '/assets/js/pace.min.js', array(), '1.0.2', false );
    wp_enqueue_style( 'pace', get_template_directory_uri() . '/assets/css/pace-theme-minimal.css', array(), '1.0.2', 'all');

    wp_enqueue_style( 'index-css', get_template_directory_uri() . '/assets/css/index.css', array(), '1.0.0', 'all');
    wp_enqueue_script( 'index-js', get_template_directory_uri() . '/assets/js/index.min.js', array(), '1.0.0', true );

    // Localize the script with new data
    $localize_array = array(
        'blogInfoSite' => get_bloginfo( 'name' ),
        'is_mobile' => wp_is_mobile(),
        'ajaxUrl' => admin_url( 'admin-ajax.php' ),
        'siteUrl' => get_site_url()
    );
    wp_localize_script( 'index-js', 'wp', $localize_array );
}
add_action( 'wp_enqueue_scripts', 'music_theme_scripts' );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images.
 *
 * @since 1.0.0
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function music_theme_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 740 <= $width ) {
		$sizes = '(max-width: 706px) 89vw, (max-width: 767px) 82vw, 740px';
	}

	if ( is_active_sidebar( 'sidebar-1' ) || is_archive() || is_search() || is_home() || is_page() ) {
		if ( ! ( is_page() && 'one-column' === get_theme_mod( 'page_options' ) ) && 767 <= $width ) {
			 $sizes = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
		}
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'music_theme_content_image_sizes_attr', 10, 2 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param array $attr       Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size       Registered image size or flat array of height and width dimensions.
 * @return string A source size value for use in a post thumbnail 'sizes' attribute.
 */
function music_theme_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( is_archive() || is_search() || is_home() ) {
		$attr['sizes'] = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
	} else {
		$attr['sizes'] = '100vw';
	}

	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'music_theme_post_thumbnail_sizes_attr', 10, 3 );

/**
 * Use front-page.php when Front page displays is set to a static page.
 *
 * @since 1.0.0
 * @param string $template front-page.php.
 * @return string The template to be used: blank if is_home() is true (defaults to index.php), else $template.
 */
function music_theme_front_page_template( $template ) {
	return is_home() ? '' : $template;
}
add_filter( 'frontpage_template',  'music_theme_front_page_template' );

/**
 * Implement the Custom Header feature.
 */
require get_parent_theme_file_path( '/inc/custom-header.php' );

/**
 * Add custom class to active menu and dropdown.
 * @param array $classes
 * @param object $item
 * @return array
 */
function special_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }

    if ( in_array('menu-item-has-children', $classes) ) {
        $classes[] = 'dropdown ';
    }

    return $classes;
}

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

/**
 * Add custom attribute with ID
 * @param $atts
 * @param $item
 * @param $args
 * @return mixed
 */
function add_specific_menu_atts( $atts, $item, $args ) {
    $atts['data-id'] = $item->object_id;
    return $atts;
}

add_filter( 'nav_menu_link_attributes', 'add_specific_menu_atts', 10, 3 );

/**
 * Add custom class to ul element form navigation menu
 * @param $menu
 * @return mixed
 */
function new_submenu_class($menu) {
    $menu = preg_replace('/ class="sub-menu"/','/ class="dropdown-menu" /',$menu);
    return $menu;
}

add_filter('wp_nav_menu','new_submenu_class');

/**
 * Add the Facebook JavaScript SDK for comments on page.
 */
function add_facebook_sdk() {
    ?>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=404912609866249";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <?php
}

add_action('wp_head', 'add_facebook_sdk');

/**
 * Add Open Graph DOCTYPE.
 */
function add_opengraph_doctype( $output ) {
    return $output . '
    xmlns="https://www.w3.org/1999/xhtml"
    xmlns:og="https://ogp.me/ns#" 
    xmlns:fb="http://www.facebook.com/2008/fbml"';
}

add_filter('language_attributes', 'add_opengraph_doctype');

/**
 * Add Open Graph Meta Info from the actual article data.
 */
function openGraphMeta() {
    global $post;

    if ( !is_singular() ) //if it is not a post or a page
        return;

    if ( preg_match( '|<p>(.*?)</p>|', $post->post_content, $matches) ) {
        $excerpt = strip_tags( $matches[0] );
        $excerpt = str_replace("", "'", $excerpt);
    } else {
        $excerpt = get_bloginfo('description');
    }

    echo '<meta property="og:title" content="' . get_the_title() . '"/>';
    echo '<meta property="og:description" content="' . $excerpt . '"/>';
    echo '<meta property="og:type" content="music.playlist"/>';
    echo '<meta property="og:url" content="' . get_permalink() . '"/>';
    echo '<meta property="og:site_name" content="' . get_bloginfo( "description" ). '"/>';

    if( !has_post_thumbnail( $post->ID ) ) { //the post does not have featured image, use a default image
        //Create a default image on your server or an image in your media library, and insert it's URL here
        $default_image = get_bloginfo( 'stylesheet_directory' ) . '/assets/images/no-cover-image.png';
        echo '<meta property="og:image" content="' . $default_image . '"/>';
    } else {
        $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'music_theme-cover' );
        echo '<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>';
        echo '<meta property="og:image:width" content="200"/>';
        echo '<meta property="og:image:height" content="200"/>';
    }

    echo '<meta property="fb:app_id" content="404912609866249"/>';

    echo "
	";
}
add_action( 'wp_head', 'openGraphMeta', 5 );

