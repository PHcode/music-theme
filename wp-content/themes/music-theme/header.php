<?php
/**
 * Header template.
 *
 * @package MusicTheme
 * @since 1.0.0
 * @version 1.0.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <?php wp_head(); ?>
    </head>
<body <?php body_class(); ?>>
    <!-- Main header -->
    <div class="main-header">
        <?php the_custom_header_markup(); ?>
        <?php if ($headerColor = get_header_textcolor() ) {
            $styleHeader = 'style="color:#' . $headerColor . '"';
        } else {
            $styleHeader = '';
        } ?>
        <header class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <div class="data-site">
                        <div class="logo-header">
                            <?php if ( function_exists( 'the_custom_logo' ) ) :
                                the_custom_logo();
                            endif; ?>
                        </div>
                        <div class="name-description" <?php echo $styleHeader; ?>>
                            <?php if ( function_exists( 'the_custom_logo' ) ) : ?>
                                <p><?php bloginfo('description'); ?></p>
                            <?php else: ?>
                                <h1><?php bloginfo( 'name' ); ?></h1>
                                <p><?php bloginfo('description'); ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <form class="search-form-header" role="search" method="get" id="searchFormOne" action="<?php echo get_site_url(); ?>">
                        <div class="form-group">
                            <label for="search"><?php esc_html_e( 'Search by Artist - Song', 'music_theme' ); ?></label>
                            <input type="search" class="form-control" name="s" id="s" placeholder="Search by Artist - Song">
                        </div>
                        <button type="submit" class="btn btn-default submit" id="searchsubmit">Search</button>
                    </form>
                </div>
            </div>
        </header>
        <!-- Fixed header -->
        <?php if ( is_user_logged_in() ) :
            $topHeader = '32px';
            if ( wp_is_mobile() ) :
                $topHeader = '0';
            endif;
        else:
            $topHeader = '0';
        endif; ?>
        <div class="header-fixed" style="top: <?php echo $topHeader; ?>">
            <div class="logo-header">
                <?php if ( function_exists( 'the_custom_logo' ) ) :
                    the_custom_logo();
                else:
                    echo "<h1>" . get_bloginfo( 'name' ) . "</h1>";
                endif; ?>
                <p class="site-description hidden-xs">
                    <?php bloginfo('description'); ?>
                </p>
            </div>
            <form class="search-form-header" role="search" method="get" id="searchFormTwo" action="<?php echo get_site_url(); ?>">
                <div class="form-group">
                    <label for="search"><?php esc_html_e( 'Search by Artist - Song', 'music_theme' ); ?></label>
                    <input type="search" class="form-control" name="search" id="search" placeholder="Search by Artist - Song">
                </div>
                <button type="submit" class="btn btn-default submit" id="searchsubmit"><i class="fa fa-search visible-xs" aria-hidden="true"></i><span class="hidden-xs">Search</span></button>
            </form>
        </div><!-- ./Fixed header -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-default main-nav">
                        <?php if ( has_nav_menu( 'top' ) ) : ?>
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header visible-xs">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <!-- Collect the nav links and other content for toggling -->
                            <?php wp_nav_menu( array(
                                'container_class' => 'collapse navbar-collapse',
                                'container_id' => 'bs-example-navbar-collapse-1',
                                'menu_class' => 'nav navbar-nav',
                                'theme_location' => 'top',
                                'menu_id'        => 'top-menu'
                            ) ); ?>
                            <!-- /.navbar-collapse -->
                        <?php endif; ?>
                    </nav>
                    <div class="player-data">
                        <div class="time-lapsed">
                            <span class="hours">00:</span><span class="minutes">00</span>:<span class="seconds">00</span>
                        </div>
                        <div class="song-name">
                            Song name
                        </div>
                        <div class="time-total">
                            00:00:00
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

