<?php
/**
 * All ajax requests.
 *
 * @author Fernando López Moreno phantomcode@outlook.com
 * @package MusicTheme
 * @since 1.0.0
 * @version 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'Music_theme_request_ajax' ) ) {
    class Music_theme_request_ajax {
        public static function init() {
            // Hook Show recent search
            add_action( 'wp_ajax_searchPage',  array( __CLASS__, 'searchPage' ) );
            add_action( 'wp_ajax_nopriv_searchPage', array( __CLASS__, 'searchPage' ) );

            // Hook home page
            add_action( 'wp_ajax_homePage',  array( __CLASS__, 'homePage' ) );
            add_action( 'wp_ajax_nopriv_homePage', array( __CLASS__, 'homePage' ) );

            // Hook page page
            add_action( 'wp_ajax_pagePage',  array( __CLASS__, 'pagePage' ) );
            add_action( 'wp_ajax_nopriv_pagePage', array( __CLASS__, 'pagePage' ) );
        }

        public static function searchPage() {
            $jsondata = array();
            $word = $_POST['word'];
            $paged = $_POST['paged'];
            $query = new WP_Query( array(
                's' => $word,
                'paged' => $paged,
                'post_type' => 'post'
            ) );
            $GLOBALS['wp_query'] = $query;
            ob_start(); ?>
            <section class="content-search">
                <header class="header-search">
                    <?php if ( $query->have_posts() ) : ?>
                        <h1 class="title-search">
                            <?php printf( __( 'Search results for: %s', 'music_theme' ), '<span>' . $word . '</span>' ); ?>
                        </h1>
                    <?php else : ?>
                        <h1 class="title-search">
                            <?php _e( 'Nothing Found', 'music_theme' ); ?>
                        </h1>
                    <?php endif; ?>
                </header>
                <?php if ( $query->have_posts() ) : ?>
                    <?php while ( $query->have_posts() ) : $query->the_post();
                        get_template_part( 'templates/content', 'search' );
                    endwhile;
                    the_posts_pagination( array(
                        'prev_text' => '<i class="fa fa-angle-double-left" aria-hidden="true"></i>',
                        'next_text' => '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
                        'before_page_number' => '',
                    ) );
                else: ?>
                    <p class="nothing-matched">
                        <?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'music_theme' ); ?>
                    </p>
                <?php endif; ?>
            </section>
            <?php $jsondata['content'] = ob_get_contents();
            ob_end_clean();

            echo json_encode($jsondata);
            die();
        }

        public static function homePage() {
            $jsondata = array();
            $paged = $_POST['paged'];
            $query = new WP_Query( array(
                'paged' => $paged,
                'post_type' => 'post'
            ) );
            $GLOBALS['wp_query'] = $query;
            ob_start(); ?>
            <section class="content-home">
                <?php if ( $query->have_posts() ) :
                    /* Start the Loop */
                    while ( $query->have_posts() ) : $query->the_post();
                        get_template_part( 'templates/content', 'home' );
                    endwhile;
                    the_posts_pagination( array(
                        'prev_text' => '<i class="fa fa-angle-double-left" aria-hidden="true"></i>',
                        'next_text' => '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
                        'before_page_number' => '',
                    ) );
                else:
                    get_template_part( 'templates/content', 'none' );
                endif; ?>
            </section>
            <?php $jsondata['content'] = ob_get_contents();
            ob_end_clean();

            echo json_encode($jsondata);
            die();
        }

        public static function pagePage() {
            $jsondata = array();
            $pageId = $_POST['pageId'];
            $query = new WP_Query( array(
                'page_id' => $pageId
            ) );
            ob_start(); ?>
            <div class="content-page">
                <?php
                while ( $query->have_posts() ) : $query->the_post();
                    get_template_part( 'templates/content', 'page' );
                endwhile; ?>
            </div>
            <?php $jsondata['content'] = ob_get_contents();
            ob_end_clean();
            $jsondata['permalink'] = get_permalink( $pageId );
            echo json_encode($jsondata);
            die();
        }
    }
}

Music_theme_request_ajax::init();