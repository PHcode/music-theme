<?php
/**
 * Music Theme back compat functionality
 *
 * Prevents Music Theme from running on WordPress versions prior to 4.7,
 * since this theme is not meant to be backward compatible beyond that and
 * relies on many newer functions and markup changes introduced in 4.7.
 *
 * @package MusicTheme
 * @since 1.0.0
 * @version 1.0.0
 */

/**
 * Prevent switching to Music Theme on old versions of WordPress.
 *
 * Switches to the default theme.
 *
 * @since 1.0.0
 */
function music_theme_switch_theme() {
	switch_theme( WP_DEFAULT_THEME );
	unset( $_GET['activated'] );
	add_action( 'admin_notices', 'music_theme_upgrade_notice' );
}
add_action( 'after_switch_theme', 'music_theme_switch_theme' );

/**
 * Adds a message for unsuccessful theme switch.
 *
 * Prints an update nag after an unsuccessful attempt to switch to
 * Music Theme on WordPress versions prior to 4.7.
 *
 * @since 1.0.0
 * @global string $wp_version WordPress version.
 */
function music_theme_upgrade_notice() {
	$message = sprintf( __( 'Music Theme requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'music_theme' ), $GLOBALS['wp_version'] );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

/**
 * Prevents the Customizer from being loaded on WordPress versions prior to 4.7.
 *
 * @since 1.0.0
 * @global string $wp_version WordPress version.
 */
function music_theme_customize() {
	wp_die( sprintf( __( 'Music Theme requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'music_theme' ), $GLOBALS['wp_version'] ), '', array(
		'back_link' => true,
	) );
}
add_action( 'load-customize.php', 'music_theme_customize' );

/**
 * Prevents the Theme Preview from being loaded on WordPress versions prior to 4.7.
 *
 * @since 1.0.0
 * @global string $wp_version WordPress version.
 */
function music_theme_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die( sprintf( __( 'Music Theme requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'music_theme' ), $GLOBALS['wp_version'] ) );
	}
}
add_action( 'template_redirect', 'music_theme_preview');
