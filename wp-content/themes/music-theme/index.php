<?php
/**
 * Index page.
 * @package MusicTheme
 * @since 1.0.0
 * @version 1.0.0
 */
get_header(); ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="main-content">
                    <section class="content-home">
                        <?php if ( have_posts() ) :
                            /* Start the Loop */
                            while ( have_posts() ) : the_post();
                                get_template_part( 'templates/content', 'home' );
                            endwhile;
                            the_posts_pagination( array(
                                'prev_text' => '<i class="fa fa-angle-double-left" aria-hidden="true"></i>',
                                'next_text' => '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
                                'before_page_number' => '',
                            ) );
                        else:
                            get_template_part( 'templates/content', 'none' );
                        endif; ?>
                    </section>
                </div>
            </div>
        </div>
    </div>
<?php get_footer();
