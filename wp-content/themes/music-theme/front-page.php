<?php
/**
 * The front page template file
 *
 * @package MusicTheme
 * @since 1.0.0
 * @version 1.0.0
 */
get_header(); ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="main-content">
                    <!-- Content page -->
                    <div class="content-front">
                        <?php
                        while ( have_posts() ) : the_post();
                            get_template_part( 'templates/content', 'page' );
                        endwhile; ?>
                    </div><!-- ./Content single -->
                </div>
            </div>
        </div>
    </div>
<?php get_footer();