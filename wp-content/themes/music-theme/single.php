<?php
/**
 * The template for displaying all single posts.
 * @package MusicTheme
 * @since 1.0.0
 * @version 1.0.0
 */
get_header(); ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="main-content">
                    <!-- Content single -->
                    <div class="content-single">
                        <?php
                        while ( have_posts() ) : the_post();
                            get_template_part( 'templates/content', 'single' );
                        endwhile; ?>
                    </div><!-- ./Content single -->
                    <!-- Facebook comments -->
                    <div class="fb-comments-container">
                        <div class="fb-comments" data-href="<?php the_permalink(); ?>" width="100%" data-numposts="5"></div>
                    </div><!-- ./Facebook comments -->
                </div>
            </div>
        </div>
    </div>
<?php get_footer();
